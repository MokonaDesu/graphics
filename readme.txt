# Setup process
1. Download and install https://www.python.org/ftp/python/2.7.12/python-2.7.12.msi
    !!! MAKE SURE TO ADD PYTHON TO YOUR PATH !!!
    ( There will be an appropariate option in the 'Choose components to install' window )

2. Download and install https://www.microsoft.com/en-us/download/details.aspx?id=44266

3. Download FREEGLUT: http://files.transmissionzero.co.uk/software/development/GLUT/freeglut-MSVC.zip
   Unzip the thing and copy the contents of the 'BIN' folder to your python installation folder (by default, C:\Python27)

4. open up your command line (cmd) and run:
    python -m pip install --upgrade pip

    pip install PyOpenGL PyOpenGL_accelerate

    ( If windows tells you that there is no such program as 'pip' or 'python',
    then you've failed to add python to your PATH. Go ahead and reinstall.)

4. You are done. Run
    python lab01.py
