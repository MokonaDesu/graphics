import sys

from OpenGL.GL import *
from OpenGL.GLUT import *
from OpenGL.GLU import *

import utility

class GlutRenderer:
    def __init__(self, window_title, console_logging = False):
        self.__frame_count = 0
        self.__console_logging = console_logging
        glutInit(sys.argv)
        glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH)
        glutInitWindowSize(640, 480)
        glutInitWindowPosition(0, 0)
    	self.__window = glutCreateWindow(window_title)
       	glutDisplayFunc(self.__render_screen)
        glutIdleFunc(self.__render_screen)
        glutReshapeFunc(self.__resize_handler)
    	glutKeyboardFunc(self.__keypress_handler)
    	self.__initialize_gl(640, 480)
    	glutMainLoop()

    def __initialize_gl(self, width, height):
    	glClearColor(0.0, 0.0, 0.0, 0.0)
    	glClearDepth(1.0)
    	glDepthFunc(GL_LESS)
    	glEnable(GL_DEPTH_TEST)
    	glShadeModel(GL_SMOOTH)
    	glMatrixMode(GL_PROJECTION)
    	glLoadIdentity()
    	gluPerspective(45.0, float(width)/float(height), 0.1, 100.0)
    	glMatrixMode(GL_MODELVIEW)

    def __resize_handler(self, width, height):
    	if height == 0:
    		height = 1

    	glViewport(0, 0, width, height)
    	glMatrixMode(GL_PROJECTION)
    	glLoadIdentity()
    	gluPerspective(45.0, float(width)/float(height), 0.1, 100.0)
    	glMatrixMode(GL_MODELVIEW)

    def __render_screen(self):
        self.__frame_count += 1
    	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    	glLoadIdentity()
        try:
            self.render_scene(self.__frame_count)
        except Exception, e:
            if self.__console_logging:
                print('Dropped frame %d (%s)' % (self.__frame_count,  e))
        glutSwapBuffers()

    def __keypress_handler(*args):
        if args[1] == utility.ESCAPE_KEY:
            print('Sayonara~~~!')
            sys.exit()

    def render_scene(self):
        raise Exception('Not implemented')
