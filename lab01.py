#!/usr/bin/python

from OpenGL.GL import *
from lib.glut_renderer import GlutRenderer

import math

class ProceduralGraphicsRenderer(GlutRenderer):
	def render_scene(self, frame_count):
		glColor3f(1.0, 1.0, 1.0)
		phase = (frame_count % 60) / 60.0
		glTranslatef(0.0, 0.0, -6.0 + 2 * math.sin(phase * 2 * math.pi))
		glBegin(GL_POLYGON)
		glVertex3f(0.0, 1.0, 0.0)
		glVertex3f(1.0, -1.0, 0.0)
		glVertex3f(-1.0, -1.0, 0.0)
		glEnd()

ProceduralGraphicsRenderer('Lab01 - Procedural Graphics (ESC to quit)', True)
